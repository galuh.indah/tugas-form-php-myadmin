<?php
    //Koneksi Database
    $server = "localhost";
    $user = "root";
    $pass = "";
    $database = "jadwal kelas";


    if(isset($_POST['bsimpan'])) //Jika Tombol Simpan Diklik
    {
        $simpan = mysqli_query($koneksi, "INSERT INTO tjadwal_kelas (id kelas, id dosen, kelas, jadwal, matkul)
                                        VALUES '$_Post[id kelas]',
                                               '$_POST[id dosen]',
                                               '$_POST[kelas]',
                                               '$_POST[jadwal]',
                                               '$_POST[]'
                                         ");
        if($simpan)
        {
            echo "<script>
                    alert('Simpan Data Sukses');
                    document.location='index.php';
                  </script>";
        } 
        else
        {
            echo "<script>
                    alert('Simpan Data Gagal!');
                    document.location='index.php';
                  </script>";
        }
    }

?>


<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-Edge">
    <meta name="viewport" content="width=device-width, initial-scalec">
    <title>Kadek Galuh Indah Kharismayanti</title> <!-- Mengisi judul di web -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>

    <a target="_blank" href="jadwal_kelas.php"> Halaman Selanjutnya</a>

<center> <!-- Agar tulian menjorok ketengah -->
<h1>SISTEM INFORMASI</h1>
<h2>PENJADWALAN DOSEN</h2>
</center>
<div class="container">

    <!-- Awal Card Form -->
    <div class="card mt-3"> <!-- Mengatur jarak atas dan bawah -->
      <div class="card-header bg-dark text-white"> <!-- Menu navigasi -->
        JADWAL KELAS 
    </div>
    <div class="card-body">
        <form method="post" action="">
            <div class="form-group" >
                <label for="id jadwal" class="col-sm-2 col-form-label">ID JADWAL</label>
                <div class="form-mb-20">
                    <input type="text" class="form-control" id="id jadwal" name="id jadwal" placeholder="Input ID">
                    </div>
                </div>
                <label for="id dosen" class="col-sm-2 col-form-label">ID DOSEN</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="id dosen" name="id dosen" placeholder="Input ID">
                    </div>
                <label for="id kelas" class="col-sm-2 col-form-label">KELAS</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="id kelas" name="id kelas" placeholder="Input ID">
                    </div>
                <label for="jadwal" class="col-sm-2 col-form-label">JADWAL</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="jadwal" name="jadwal" placeholder="Input jadwal">
                    </div>
                <label for="matkul" class="col-sm-2 col-form-label">MATA KULIAH
                    <select class="form-select" id="matkul" name="matkul" aria-label="Default select example">
                        <option selected></option>
                        <option value="Pemrograman Web">Pemrograman Web</option>
                        <option value="Pemrograman Mobile">Pemrograman Mobile</option>
                        <option value="Testing dan Implementasi">Testing dan Implementasi</option>
                        <option value="Manajemen Proyek">Manajemen Proyek</option> 
                    </select>
                    </div><br>
                <div class="d-grid gap-12">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div><br>
                <div class="d-grid gap-12">
                <button type="reset" class="btn btn-danger">Reset</button>
                </div>
        </form>
     </div>
    </div>
    <!-- Akhir Card Form -->
   
    <!-- Awal Card Tabel -->
    <div class="card mt-3">
      <div class="card-header bg-success text-white">
        DATA JADWAL KELAS 
    </div>
    <div class="card-body">

            <table class="table table-bordered table-striped">
                <tr>
                    <th>No</th>
                    <th>Id Dosen</th>
                    <th>Id Kelas</th>
                    <th>Id Jadwal</th>
                    <th>Jadwal</th>
                    <th>Mata Kuliah</th>
                    <th>Aksi</th>

                </tr>
                <?php
                    $no = 1;
                    $tampil = mysqli_query($koneksi, "SELECT * from tjadwal_kelas order by dosen desc");
                    while($data = mysqli_fetch_array($tampil)) :

                ?>
                <tr> <!-- Tabel data-->
                    <td><?=$no++;?></td>  <!-- Tabel menambahkan nomor -->
                    <td><?=$data['id dosen'];?></td> <!-- Menampilkan data dari data nip dosen -->
                    <td><?=$data['id kelas'];?></td>
                    <td><td><?=$data['id jadwal'];?></td>
                    <td><td><?=$data['jadwal'];?></td>
                    <td><td><?=$data['mata kuliah'];?></td>
                    <td>
                        <a href="#" class="btn btn-warning"> Edit </a>
                        <a href="#" class="btn btn-denger"> Hapus </a>
                    </td>
                </tr>
                <?php endwhile; ?> <!-- Penutup perulangan while -->
                <tr>
                    <td>1</td>
                    <td>282638229</td>
                    <td>SI 4A</td>
                    <td>1025</td>
                    <td>Senin, Kamis</td>
                    <td>Pemrograman Web</td>
                    <td>
                        <a href="#" class="btn btn-warning"> Edit </a>
                        <a href="#" class="btn btn-danger"> Hapus </a>
                    </td>
                </tr>
            </table>
        
     </div>
    </div>
    <!-- Akhir Card Tabel -->

</div>

<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>