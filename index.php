<?php
    //Koneksi Database
    $server = "localhost";
    $user = "root";
    $pass = "";
    $database = "jadwal dosen";

    $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

    if(isset($_POST['bsimpan'])) //Jika Tombol Simpan Diklik
    {
        $simpan = mysqli_query($koneksi, "INSERT INTO tdosen (nip, nama, prodi, fakultas, waktu)
                                        VALUES '$_Post[nip]','$_POST[nama]','$_POST[prodi]','$_POST[fakultas]','$_POST[waktu]'
                                         ");
        if($simpan)
        {
            echo "<script>
                    alert('Simpan Data Sukses');
                    document.location='index.php';
                  </script>";
        } 
        else
        {
            echo "<script>
                    alert('Simpan Data Gagal!');
                    document.location='index.php';
                  </script>";
        }
    }

?>


<!DOCTYPE html>
<html>
<head>
    <title>Kadek Galuh Indah Kharismayanti</title> <!-- Mengisi judul di web -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>

<center> <!-- Agar tulian menjorok ketengah -->
<h1>SISTEM INFORMASI</h1>
<h2>PENJADWALAN DOSEN</h2>
</center>
<div class="container">

    <!-- Awal Card Form -->
    <div class="card mt-3"> <!-- Mengatur jarak atas dan bawah -->
      <div class="card-header bg-dark text-white"> <!-- Menu navigasi -->
        JADWAL DOSEN 
    </div>
    <div class="card-body">
        <form method="post" action="">
            <div class="form-group" >
                <label for="id dosen" class="col-sm-2 col-form-label">ID DOSEN</label>
                <div class="form-mb-20">
                    <input type="text" class="form-control" id="id dosen" name="id dosen" placeholder="Input ID">
                    </div>
                </div>
                <label for="foto" class="col-sm-2 col-form-label">FOTO</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="foto" name="foto" placeholder="Input Url Foto">
                    </div>
                <label for="nip" class="col-sm-2 col-form-label">NIP</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="nip" name="nip" placeholder="Input Nip">
                    </div>
                <label for="nama" class="col-sm-2 col-form-label">NAMA</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Input Nama">
                    </div>
                <label for="prodi" class="col-sm-2 col-form-label">PRODI</label>
                <div class="form-mb-1">
                    <select class="form-select" id="podi" name="prodi" aria-label="Default select example">
                        <option selected></option>
                        <option value="Sistem Informasi">Sistem Informasi</option>
                        <option value="Ilmu Komputer">Ilmu Komputer</option>
                        <option value="Pendidikan Teknik Informatika">Pendidikan Teknik Informatika</option>
                        <option value="Manajemen Informatika">Manajemen Informatika</option> 
                    </select>
                <label for="fakultas" class="col-sm-2 col-form-label">FAKULTAS</label>
                <div class="form-mb-1">
                    <input type="text" class="form-control" id="fakultas" name="fakultas" placeholder="Input Fakultas">
                    </div><br>
                <div class="d-grid gap-12">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div><br>
                <div class="d-grid gap-12">
                <button type="reset" class="btn btn-danger">Reset</button>
                </div>
        </form>
     </div>
    </div>
    <!-- Akhir Card Form -->
   
    <!-- Awal Card Tabel -->
    <div class="card mt-3">
      <div class="card-header bg-success text-white">
        DATA JADWAL KELAS 
    </div>
    <div class="card-body">

            <table class="table table-bordered table-striped">
                <tr>
                    <th>No</th>
                    <th>Nip</th>
                    <th>Nama</th>
                    <th>Prodi</th>
                    <th>Fakultas</th>
                    <th>Waktu</th>
                    <th>Aksi</th>

                </tr>
                <?php
                    $no = 1;
                    $tampil = mysqli_query($koneksi, "SELECT * from tdosen order by dosen desc");
                    while($data = mysqli_fetch_array($tampil)) :

                ?>
                <tr> <!-- Tabel data-->
                    <td><?=$no++;?></td>  <!-- Tabel menambahkan nomor -->
                    <td><?=$data['nip'];?></td> <!-- Menampilkan data dari data nip dosen -->
                    <td><?=$data['nama'];?></td>
                    <td><td><?=$data['prodi'];?></td>
                    <td><td><?=$data['fakultas'];?></td>
                    <td><td><?=$data['waktu'];?></td>
                    <td>
                        <a href="#" class="btn btn-warning"> Edit </a>
                        <a href="#" class="btn btn-denger"> Hapus </a>
                    </td>
                </tr>
                <?php endwhile; ?> <!-- Penutup perulangan while -->
                <tr>
                    <td>1</td>
                    <td>4578975432577</td>
                    <td>Made Ria</td>
                    <td>Ilmu Komputer</td>
                    <td>Teknik dan Kejuruan</td>
                    <td>J-L</td>
                    <td>
                        <a href="#" class="btn btn-warning"> Edit </a>
                        <a href="#" class="btn btn-danger"> Hapus </a>
                    </td>
                </tr>
            </table>
        
     </div>
    </div>
    <!-- Akhir Card Tabel -->

</div>

<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>